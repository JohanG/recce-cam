var main = func(addon) {

    var addonMessagePrefix = "Recce-cam:  ";

    print(addonMessagePrefix ~ "Addon loading");

    var addonPropRoot       = "/addons/by-id/" ~ addon.id;

    # Camera settings leaf nodes
    var currentAircraftNode     = props.globals.getNode(addonPropRoot ~ "/status/current-aircraft", 1);
    var currentCameraNode       = props.globals.getNode(addonPropRoot ~ "/status/current-camera", 1);

    var currentFovNode          = props.globals.getNode("/sim/current-view/field-of-view", 1);
    var simFrameCountNode       = props.globals.getNode("/sim/rendering/sim-frame-count", 1);

    var preCaptureFov = nil;
    var lensFov = nil;
    var exposureFrame = nil;

#    var calcLensFov = func() {
#        return 2 * math.atan2(frameLengthNode.getValue()/2, focalLengthNode.getValue()) * 180/math.pi;
#    };

    var triggerFixedFovExposure = func() {
        preCaptureFov = view.fovProp.getValue();
        lensFov = calcLensFov();
        view.fovProp.setDoubleValue(lensFov);
        waitForFovBeforeExposureTimer.start();
    };

    var fixedFovExposure = func() {
        if (currentFovNode.getValue() == lensFov) {
            exposureFrame = simFrameCountNode.getValue();
            fgcommand("screen-capture");
            waitAFewFramesAfterExposureTimer.start();
            waitForFovBeforeExposureTimer.stop();
        };
    };

    var returnFovAfterExposure = func() {
        if (simFrameCountNode.getValue() > (exposureFrame)) {
            view.fovProp.setDoubleValue(preCaptureFov);
            waitAFewFramesAfterExposureTimer.stop();
        };
    };

    var screenCapture = func() {
        triggerFixedFovExposure();
        print(addonMessagePrefix ~ "Frame exposed");
    };

    var wasShutterPressed = func() {
        if (shutterStatusNode.getValue() == 1) {
            screenCapture();
            shutterStatusNode.setBoolValue(0);
        };
    };

    var waitForFovBeforeExposureTimer    = maketimer(0, fixedFovExposure);
    var waitAFewFramesAfterExposureTimer = maketimer(0, returnFovAfterExposure);

#    var shutterStatusHasChanged          = setlistener(shutterStatusNode, wasShutterPressed, 0, 0);

    print(addonMessagePrefix ~ "Addon loaded");
}

var unload = func(addon) {
    print(addonMessagePrefix ~ "Addon unloaded");
};
